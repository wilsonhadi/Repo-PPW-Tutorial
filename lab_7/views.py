from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    page = request.GET.get('page', 1)
    resp = csui_helper.instance.get_mahasiswa_list(page)
    mahasiswa_list = resp['results']
    friend_list = Friend.objects.all()
    response = {"author":"Samuel Tupa Febrian","current_page": page, "mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

@csrf_exempt
def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar-teman.html'
    return render(request, html, response)

@csrf_exempt
def get_friend_list(request):
    return JsonResponse(dict(friend_list=list(Friend.objects.values())))

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        is_taken = Friend.objects.filter(npm__iexact=npm).exists()
        if not is_taken:
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    status = False
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        status = True
    return JsonResponse({'status':status})


@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm__iexact=npm).exists() #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

@csrf_exempt
def get_list_at_page(request):
    if request.method == 'POST':
        page = request.POST['page']
        listAtPage = csui_helper.instance.get_mahasiswa_list(page)['results']
        return JsonResponse({'listOfMhs': json.dumps(listAtPage)})
